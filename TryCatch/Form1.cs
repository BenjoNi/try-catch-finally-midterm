﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryCatch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public Decimal Total;
        public Decimal NumberOfSubject;
        public Double Result;

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                Total = numericUpDown1.Value;
                NumberOfSubject = numericUpDown2.Value;

                Double Result = (decimal.ToDouble(Total) / decimal.ToDouble(NumberOfSubject));
                textBox1.Text = Result.ToString();
            }
            catch (Exception) { MessageBox.Show("Something went wrong "); }

            finally
            {
                MessageBox.Show("Done");
            }
        }
    }
}
